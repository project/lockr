<?php

namespace Drupal\lockr\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drush\Commands\DrushCommands;

use Lockr\Lockr;
use Lockr\Exception\LockrApiException;


use Drupal\lockr\CertWriter;
use Drupal\lockr\SettingsFactory;

/**
 * Class LockrDrushCommands
 * 
 * @package Drupal\lockr\Commands
 */
class LockrDrushCommands extends DrushCommands {
    /**
     * Lockr library client.
     *
     * @var Lockr
     */
    protected $lockr;

    /**
     * Lockr settings factory.
     *
     * @var SettingsFactory
     */
    protected $settingsFactory;

    /**
     * Simple config factory.
     *
     * @var ConfigFactoryInterface
     */
    protected $configFactory;

    /**
   * Constructs a new LockrRegisterForm.
   *
   * @param Lockr $lockr
   *   The Lockr library client.
   * @param SettingsFactory $settings_factory
   *   The settings factory.
   * @param ConfigFactoryInterface $config_factory
   *   The simple config factory.
   */
  public function __construct(
    Lockr $lockr,
    SettingsFactory $settings_factory,
    ConfigFactoryInterface $config_factory
  ) {
    $this->lockr = $lockr;
    $this->settingsFactory = $settings_factory;
    $this->configFactory = $config_factory;
  }

    /**
     * Drush command for registering a site via site token
     * 
     * @param string $token
     *      The client registration token
     * @option client_label 
     *      Only necessary if creating a new KeyRing. Ideal to be descriptive.
     * @option keyring_label 
     *      Only necessary if creating a new KeyRing. Ideal to be descriptive.
     * 
     * @command lockr:register-token
     * @aliases lockr-register-token lrt
     * @usage lockr:register-token 123456789 --client_label=DrushClient --keyring_label=NewKeyRingName
     */

    public function register_token($token, $options = ['client_label' => 'DrushAutomation', 'keyring_label' => NULL]) {
        $keyring_label = $options['keyring_label'];
        $client_label = $options['client_label'];
        try {
            $info = $this->lockr->getInfo();
          }
          catch (LockrApiException $e) {
            if ($e->getCode() >= 500) {
                $this->output()->writeln('The Lockr API experienced an error, please try again.');
                return;
            }
            $info = [];
          }
        if(!empty($info)) {
            $this->output()->writeln('Site is already connected to the ' . $info['keyring']['label'] . ' KeyRing.');
            return;
        }
        //If there's no token there's no way we can register
        if ( empty($token) ) {
            $this->output()->writeln('Invalid Token');
            return;
        }

        //Setup client token creation
        $url = 'https://accounts.lockr.io/lockr-api/sras';
        $payload = [
            'token' => $token,
            'keyring_label' => $keyring_label,
            'client_label' => $client_label,
        ];
        $data = json_encode($payload);

        //Request short lived client token
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        curl_close($ch);
        $tokenArray = json_decode($result);
        $client_token = $tokenArray->client_token;

        //If we don't have a token, no reason to keep going.
        if (empty($client_token)) {
            $error_message = $tokenArray->error;
            $this->output()->writeln($error_message);
            $this->output()->writeln('Error retrieving client token. Please check your parameters and try again.');
            return;
        }

        //Now let's register the client with the token we have.
        $partner = $this->settingsFactory->getPartner();
        try {
            if (is_null($partner)) {
                $dn = [
                'countryName' => 'US',
                'stateOrProvinceName' => 'Washington',
                'localityName' => 'Tacoma',
                'organizationName' => 'Lockr',
                ];
                $result = $this->lockr->createCertClient($client_token, $dn);
                CertWriter::writeCerts('dev', $result);
                $config = $this->configFactory->getEditable('lockr.settings');
                $config->set('custom', TRUE);
                $config->set('cert_path', 'private://lockr/dev/pair.pem');
                $config->save();
            }
            elseif ($partner['name'] === 'pantheon') {
                $this->lockr->createPantheonClient($client_token);
            }
        }
        catch (\Exception $e) {
            $this->output()->writeln('Site Registration Failed, please contact support or try again.');
            throw $e;
            return;
        }
        $this->output()->writeln('Site Successfully Registered.');
    }
}